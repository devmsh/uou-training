<?php

namespace Tests\Feature;

use App\Employee;
use App\Student;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class StudentUpdateTest extends TestCase {

    use DatabaseMigrations;

    public function test_student_can_update_his_own_profile() {
        // Arrange
        $student = factory(Student::class)->create([
            'name' => 'Mohammed'
        ]);

        // Act
        $response = $this->actingAs($student->user)
            ->putJson("/api/students/{$student->id}", [
                'name' => 'UpdatedName'
            ]);

        // Assert
        $response->assertSuccessful();

        $response->assertJson([
            "status" => "success",
            "data" => [
                "id" => $student->id,
                "name" => "UpdatedName"
            ]
        ]);

        $student = $student->fresh();
        $this->assertEquals('UpdatedName', $student->name);
    }

    public function test_student_cannot_update_others_profile() {
        // Arrange
        $student = factory(Student::class)->create();
        $otherStudent = factory(Student::class)->create([
            'name' => 'Mohammed'
        ]);

        // Act
        $response = $this->actingAs($student->user)
            ->putJson("/api/students/{$otherStudent->id}", [
                'name' => 'UpdatedName'
            ]);

        // Assert
        $otherStudent = $otherStudent->fresh();
        $this->assertNotEquals('UpdatedName', $otherStudent->name);

        $response->assertJson([
            "status" => "failed",
        ]);
    }

    public function test_admin_can_update_any_profile() {
        // Arrange
        $otherStudent = factory(Student::class)->create([
            'name' => 'Mohammed'
        ]);

        $employee = factory(Employee::class)->create([
            'is_admin' => true
        ]);

        $this->assertTrue($employee->user->isAdmin());

        // Act
        $response = $this->actingAs($employee->user)
            ->putJson("/api/students/{$otherStudent->id}", [
                'name' => 'UpdatedName'
            ]);

        // Assert
        $otherStudent = $otherStudent->fresh();
        $this->assertEquals('UpdatedName', $otherStudent->name);

        $response->assertJson([
            "status" => "success",
            "data" => [
                "id" => $otherStudent->id,
                "name" => "UpdatedName"
            ]
        ]);
    }
}
