<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('students','Api\StudentsController');

//
//Route::post('students/update',function(){
//    $id = request('id')??Auth::id();
//
//    if($id != Auth::id()){
//        return [
//            "status" => "failed",
//        ];
//    }
//    $student = \App\Student::find($id);
//    $student->name = request('name');
//    $student->save();
//
//    return [
//        "status" => "success",
//        "data" => $student
//    ];
//});
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
