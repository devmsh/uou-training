<?php

use Faker\Generator as Faker;

$factory->define(App\Employee::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'is_admin' => false,
        'user_id' => function(){
            return factory(\App\User::class)->create()->id;
        }
    ];
});
